/*!
 * \brief qrcode
 * \details Программа, публикующая строку из стандартного вывода в топик (использует внешнюю утилиту zbarcam для получения текста)
 * \author Петров И.М.
 * \version 2.00
 * \date 06.09.2014
 * LP 24.07.2014
 */



#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <iostream>
#include <string>
using namespace std;

//----------------------------------------------------------
// Главная функция программы
//----------------------------------------------------------
int main(int argc, char **argv)
{
  // Инициализация ROS
  ros::init(argc,argv,"qrcode");
  // определение точки входа
  ros::NodeHandle n;

  // Определение выходного топика
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("qrcode_data", 10);
  std_msgs::String msg;
  while(ros::ok())
  {
    cin >> msg.data;
    chatter_pub.publish(msg);
  }
  return 0;
}
