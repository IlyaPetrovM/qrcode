@rem 13.08.2013

del *.*~
del *.~*
del *.obj
del *.map
del *.lst
del *.asm
del *.eep
del *.err
del *.rom
del *__.*
del *.hex
del *.vec
del *.inc

del *.sym
del *.cof

del *.a*
del *.o*
del *.*!
del *.*@
del *.c_*
del *.cci
del *.hdr
del *.str*
del *.i

del *.h!
del *.fct
del *.*_cbf

del *.tds
