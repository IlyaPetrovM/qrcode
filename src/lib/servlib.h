/*
 * SERV library (Linux)
 * Сервисная библиотека. Много полезных функций
 * Version 1.03
 * 18.01.2014
 * LP 02.02.2014
*/

#ifndef _SERVLIB_H_
#define _SERVLIB_H_

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <fcntl.h>

typedef char l_string[512];


char *newstr(char *s);
void error(const char *fmt, ...);
void trim(char *s);

// Чтение строк. Игнорируются пустые строки и строки-комментарии (начинающиеся с символа '#')
int ReadString(FILE *f, char *s);
int ReadInt(FILE *f, int *n);
int Read2Int(FILE *f, int *n1, int *n2);
int Read2Float(FILE *f, float *f1, float *f2);
int ReadIntVector(FILE *f, int v[]);
// Чтение целочисленного вектора из строки. Формат: <количество элементов> v[0] v[1] ... 
// Возвращает количество элементов или -1 в случае ошибки

#endif
