/*
 * SERV library (Linux)
 * Сервисная библиотека. Много полезных функций
 * Version 1.03
 * 18.01.2014
 * LP 02.02.2014
*/

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "servlib.h"

char *newstr(char *s)
{
  char *v = new char[strlen(s)+1];
  strcpy(v,s);
  return v;
}

void error(const char *fmt, ...)
{
  char s[512];
  va_list argptr;
  va_start(argptr, fmt);
  vsprintf(s, fmt, argptr);
  va_end(argptr);

  fprintf(stderr,"\nError: %s\n",s);
  perror("");
  exit(1);
}

void trim(char *s)
{
  char *v = s;
  int n=strlen(v)-1;
  while((unsigned char)v[n]<=32 && n>0)
    { v[n]=0; n--; }

  while(strlen(v)>0 && ((unsigned char)v[0]<=32))
      v++;
  strcpy(s,v);
}

int find_char(char c, const char *s)
{
  int i;
  for(i=0;*s;i++) if(*s++==c) return i;
  return -1;
}

int V_scanf(FILE *point, char *msg, int len)
{
  msg[0] = 0;
  if(fgets(msg, len, point) == NULL)
  {
    if(msg[0]) return 1;
    return 0;
  }
  trim(msg);
  return 1;
}

int ReadString(FILE *f, char *s)
{
  int k, ok;
  do
  {
    if(!V_scanf(f,s,sizeof(l_string)))
    {
      s[0] = 0;
      return 0;
    }
    trim(s);
    k = find_char('#',s);
    if(k==0) s[k] = 0;
    ok = (s[0]!=0);
  } while (!ok);
  return 1;
}

int ReadInt(FILE *f, int *n)
{
  l_string s;
  if(!ReadString(f, s)) return 0;
  *n = atoi(s);
  return 1;
}

int Read2Int(FILE *f, int *n1, int *n2)
{
  l_string s;
  if(!ReadString(f, s)) return 0;
  if(sscanf(s, "%d %d", n1, n2)!=2) return 0;
  return 1;
}

int Read2Float(FILE *f, float *f1, float *f2)
{
  l_string s;
  if(!ReadString(f, s)) return 0;
  if(sscanf(s, "%f %f", f1, f2)!=2) return 0;
  return 1;
}

int ReadIntVector(FILE *f, int v[])
// Чтение целочисленного вектора из строки. Формат: <количество элементов> v[0] v[1] ... 
// Возвращает количество элементов или -1 в случае ошибки
{
  char *p;
  l_string s;
  if(!ReadString(f, s)) return -1;
  p = strtok(s, " ,");
  int n = atoi(p);
  for(int i=0;i<n;i++)
  {
    p = strtok('\0', " ,");
    if(!p) return -1;
    v[i] = atoi(p);
  }
  return n;
}
